This app is about Space Facts that implements various android widgets including ListViews, TextViews, ActionBars, Floating Action Buttons. 


There are mainly 5 sections of this app, 
   
    Space Facts: This Activity reads facts from a string array and users can navigate through each facts using swipe gestures. 
   
    Age Calculator and Weight Calculators - Uses custom Listviews and MVC Architecture to populate lists with data and images. 
   
    Fast Facts : Also uses custom listviews and a Buffer Reader to read csv file. 
   
    Cosmic Calendar - uses custom listview, MVC Architecture.

