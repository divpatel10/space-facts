package com.divyanshu.spacefacts;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class FastFactsActivity extends AppCompatActivity {

    int[] icon = new int[]{R.drawable.mercury, R.drawable.venus, R.drawable.earth, R.drawable.moon, R.drawable.mars, R.drawable.jupiter, R.drawable.saturn, R.drawable.uranus, R.drawable.neptune, R.drawable.pluto};
    int planet_pos;
    String[] planets = new String[]{"Mercury", "Venus", "Earth", "Moon", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune", "Pluto"};
    ImageView imageView;
    TextView textView;
    ListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fast_facts);

        planet_pos = getIntent().getExtras().getInt("getExtra");
        imageView = findViewById(R.id.fast_facts_icons);
        imageView.setImageResource(icon[planet_pos]);
        textView = findViewById(R.id.fast_facts_textheader);
        textView.setText(planets[planet_pos]);
        readPlanetData(R.raw.planet_sheet_metric);


    }

    //Function that reads and loads new data on the listview, function reads a file of csv format
    private void readPlanetData(int resID){
        List<String[]> facts = new ArrayList<String[]>();

        BufferedReader reader = new BufferedReader(new InputStreamReader(getResources().openRawResource(resID), StandardCharsets.UTF_8));
        while (true) {
            try {
                String readLine = reader.readLine();
                String line = readLine;
                if (readLine == null) {
                    break;
                }

                String[] token = line.split(",");
                String[] factInfo = new String[11];
                for(int i=0; i<11; i++){
                    factInfo[i] = token[i];
                }
                facts.add(factInfo);
                listView = findViewById(R.id.fast_facts_listView);
                listView.setAdapter(new FastFactsAdapter(this,facts,planet_pos));


            } catch (IOException e) {
                Log.wtf("I/O Exception", "Error Reading the file");
                e.printStackTrace();
            }
        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fast_facts, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        AlertDialog.Builder builder1;
        if (id == R.id.menu_sort) {
            builder1 = new AlertDialog.Builder(this);
            builder1.setTitle("Data Unit");
            builder1.setCancelable(true);
            builder1.setPositiveButton("Metric Units", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    readPlanetData(R.raw.planet_sheet_metric);
                    dialog.cancel();
                }
            });
            builder1.setNegativeButton("U.S. Units", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    readPlanetData(R.raw.planet_sheet_us);
                    dialog.cancel();
                }
            });
            builder1.create().show();
        } else if (id == R.id.menu_info_ff) {
            builder1 = new AlertDialog.Builder(this);
            builder1.setTitle("Author/Curator");
            builder1.setMessage("Dr. David R. Williams\n\ndave.williams@nasa.gov\n\nTaken From: https://nssdc.gsfc.nasa.gov/planetary/factsheet/");
            builder1.create().show();
        }
        return true;
    }

}