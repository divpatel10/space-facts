package com.divyanshu.spacefacts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;


public class FastFactsAdapter extends ArrayAdapter<String[]> {
    int planet;
    List<String[]> facts;



    public FastFactsAdapter( @NonNull Context context, List<String[]> a, int p) {
        super(context, 0,a);
        facts = a;
        planet = p;

    }

    @NonNull
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listViewAdapter = convertView;

        if (listViewAdapter == null) {
            listViewAdapter = LayoutInflater.from(getContext()).inflate(R.layout.list_view_fast_facts, parent, false);
        }

        ((TextView) listViewAdapter.findViewById(R.id.fast_facts_col_1)).setText(facts.get(position)[0]);
        TextView textView2 = (TextView) listViewAdapter.findViewById(R.id.fast_facts_col_2);
        textView2.setText(facts.get(position)[planet+1]);

        return listViewAdapter;
    }
}

