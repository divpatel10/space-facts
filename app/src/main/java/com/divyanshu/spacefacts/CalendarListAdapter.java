package com.divyanshu.spacefacts;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class CalendarListAdapter extends BaseAdapter {

    Boolean changedView;
    private Context mContext;
    private List<Products> mProductList;

    @Override
    public int getCount() {
        return mProductList.size();
    }

    @Override
    public Object getItem(int position) {
        return mProductList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public CalendarListAdapter(Context mContext, List<Products> mProductList) {
        this.mContext = mContext;
        this.mProductList = mProductList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        changedView = false;
        if (view == null)
            view = View.inflate(mContext, R.layout.list_view_cosmic_calendar, null);


        TextView textView = view.findViewById(R.id.textDate);

        TextView textViewDescription = view.findViewById(R.id.textDescription);
        textView.setText(String.valueOf(mProductList.get(position).getDate()));
        textViewDescription.setText(String.valueOf(mProductList.get(position).getInfo()));


        if (mProductList.get(position).getInfo() == null) {

            textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 24);
            textView.setTextColor(Color.WHITE);
            textViewDescription.setText("");


        } else {
            textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
            textView.setTextColor(Color.WHITE);


        }
        view.setTag(mProductList.get(position).getId());

        return view;

    }
}





