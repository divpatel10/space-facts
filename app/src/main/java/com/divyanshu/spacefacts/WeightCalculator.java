package com.divyanshu.spacefacts;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class WeightCalculator extends AppCompatActivity {
    private double[] planetWeights = new double[11];
    double inputWeight;
    String[] planetNamesString = {"Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune", "Pluto", "Sun", "Moon"};
    ArrayList<planet> weight_data;
    ListView listView;
    AlertDialog.Builder alertBox;
    private enum planetNames {
        MERCURY(0.377),
        VENUS(0.904),
        EARTH(1.0),
        MARS(0.378),
        JUPITER(2.527),
        SATURN(1.065),
        URANUS(0.886),
        NEPTUNE(1.14),
        PLUTO(0.063),
        SUN(27.94),
        MOON(0.165);

        private final double gravity;

        planetNames(double gravity) {
            this.gravity = gravity;

        }

        private double gravity() {
            return gravity;

        }
    }
    int[] planetImages = {R.drawable.mercury, R.drawable.venus, R.drawable.earth, R.drawable.mars, R.drawable.jupiter, R.drawable.saturn, R.drawable.uranus, R.drawable.neptune, R.drawable.pluto, R.drawable.sun, R.drawable.moon};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weight_calculator);

            FloatingActionButton fab = findViewById(R.id.weight_activity_fab);
            listView = findViewById(R.id.weight_activity_listview);
            weight_data = new ArrayList<>();



            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
            runWeightCalculator();
                }
            });

    }

    private void runWeightCalculator(){
        EditText editText = new EditText(WeightCalculator.this);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});
        alertBox = new AlertDialog.Builder(WeightCalculator.this);
        alertBox.setMessage("Enter your weight on earth (lb/kg):");
        alertBox.setTitle("Weight Calculator");
        alertBox.setView(editText);
        alertBox.setPositiveButton("Go", (dialog, which) -> {
            if(editText.getText() == null || editText.getText().toString().length() ==0){
                Toast.makeText(WeightCalculator.this.getApplicationContext(), "Please enter a weight", Toast.LENGTH_SHORT).show();
                return;
            }
            inputWeight = Double.parseDouble(editText.getText().toString());
            for(int i=0; i<11;i++){
                planetWeights[i] = planetNames.values()[i].gravity()* inputWeight;
                weight_data.add(new planet(planetNamesString[i],planetWeights[i],planetImages[i]));
            }
            set_List();

            alertBox.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        });
        alertBox.create().show();


    }
    private void set_List(){

        listView = findViewById(R.id.weight_activity_listview);
        listView.setAdapter(new PlanetAdapter(WeightCalculator.this, weight_data, 2));
    }
}