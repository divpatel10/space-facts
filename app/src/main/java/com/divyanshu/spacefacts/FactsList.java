package com.divyanshu.spacefacts;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class FactsList {
    public int n;
    List<String> facts;
    DatabaseReference databaseReference;
    Context mContext;
    SharedPreferences sharedPreferences;
    Gson gsonArrayFacts;
    FactsList(Context context){
        mContext = context;
        facts = new ArrayList<>();
        gsonArrayFacts = new Gson();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("Facts");
        getFactList();


     }

     private boolean isConnected(){
         ConnectivityManager connectivityManager
                 = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
         NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
         return activeNetworkInfo != null && activeNetworkInfo.isConnected();
     }

    //Function to save the fact List using shared Preferences
    public void SavefactList(){
         sharedPreferences = mContext.getSharedPreferences("mypref", 0);
            String tempList = gsonArrayFacts.toJson(facts);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        if(tempList!=null) {
            editor.putString("factList", tempList);
            editor.apply();
      }

    }

    public String getFact(int i) {
        String fact = "";
        n = facts.size();

        if (i < n) {
            fact = facts.get(i);
            return fact;
        } else {

            String el = "Sorry! Out of Facts!";
            return el;
        }
    }

    public int len() {
        n = facts.size();
        return n;
    }

    private void getFactList(){
        if(isConnected()){

            databaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    facts.clear();
                    for(DataSnapshot Dsnapshot : snapshot.getChildren()){
                        DataSnapshot db = Dsnapshot.child("Facts");
                        if(db.getValue(String.class)!=null){
                            facts.add(db.getValue(String.class));
                        }
                    }
                    SavefactList();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    //shared prefrences to save last viewed fact
                    Toast.makeText(mContext, "Something went wrong, please try again later", Toast.LENGTH_LONG).show();
                }
            });
        }

        else{

            sharedPreferences = mContext.getSharedPreferences("mypref", 0);
            Gson gson = new Gson();
            String jsonArrayFacts= " ";
            try {
                 jsonArrayFacts = sharedPreferences.getString("factList", " ");
            }catch (Exception e){
                Toast.makeText(mContext, "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            if(jsonArrayFacts.equals(" ")){
                Toast.makeText(mContext, "No Internet Connection", Toast.LENGTH_LONG).show();
            }
            else{

                Type type = new TypeToken<List<String>>() {
                }.getType();
                facts = gson.fromJson(jsonArrayFacts,type);

        }}
    }



}

