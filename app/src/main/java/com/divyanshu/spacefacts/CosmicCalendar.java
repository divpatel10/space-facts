package com.divyanshu.spacefacts;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CosmicCalendar extends AppCompatActivity {
    private CalendarListAdapter adapter;
    private ListView lv;
    private List<Products> mProductlist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cosmic_calendar);
        lv = findViewById(R.id.cosmicCalendar_id);
         mProductlist = new ArrayList();
         mProductlist.add(new Products(1, "1 January", "Big Bang occured."));
         mProductlist.add(new Products(2, "14 January", "Oldest known Gammma burst."));
         mProductlist.add(new Products(3, "22 January", "First galaxies formed."));
         mProductlist.add(new Products(4, "6 March", "Milky Way galaxy formed."));
         mProductlist.add(new Products(5, "12 May", "Milky Way galaxy disk formed."));
         mProductlist.add(new Products(6, "2 September", "Formation of our Solar System."));
         mProductlist.add(new Products(7, "6 September", "Oldest Known rocks on Earth."));
         mProductlist.add(new Products(8, "\nEvolution of life on Earth.", null));
         mProductlist.add(new Products(9, "14 September", "Remains of biotic life found in 4.1 billion-year-old rocks in Western Australia."));
         mProductlist.add(new Products(10, "21 September", "First Life (Prokaryotes)"));
         mProductlist.add(new Products(11, "30 September", "Some organisms started using photosysthesis to make their food. "));
         mProductlist.add(new Products(12, "29 October", "Oxygenation of atmosphere."));
         mProductlist.add(new Products(13, "9 November", "Complex cells (Eukaryotes formed.)"));
         mProductlist.add(new Products(14, "5 December", "Firt Multicelluar life formed."));
         mProductlist.add(new Products(15, "7 December", "Simple Animals."));
         mProductlist.add(new Products(16, "14 December", "Arthropods (ancestors of insects, arachnids)."));
         mProductlist.add(new Products(17, "17 December", "Fish and Proto-amphibians"));
         mProductlist.add(new Products(18, "20 December", "Land Plants."));
         mProductlist.add(new Products(19, "21 December", "Insects and seeds."));
         mProductlist.add(new Products(20, "22 December", "Amphibians"));
         mProductlist.add(new Products(21, "23 December", "Reptiles"));
         mProductlist.add(new Products(22, "24 December", "Permian-Triassic Extinction Event, 90% of Species Die Out"));
         mProductlist.add(new Products(23, "25 December", "Dinosaurs"));
         mProductlist.add(new Products(24, "26 December", "Mammals"));
         mProductlist.add(new Products(25, "27 December", "Birds"));
         mProductlist.add(new Products(26, "28 December", "Flowers"));
         mProductlist.add(new Products(27, "30 December, 06:24", "Cretaceous–Paleogene extinction event, Non-avian Dinosaurs Die Out"));
         mProductlist.add(new Products(28, "\nHuman Evolution", null));
         mProductlist.add(new Products(29, "30 December", "Primates"));
         mProductlist.add(new Products(30, "31 December, 6:05 ", "Apes"));
         mProductlist.add(new Products(31, "31 December, 14:24 ", "Hominids"));
         mProductlist.add(new Products(32, "31 December, 22:24 ", "Primitive Humans and Stone Tools"));
         mProductlist.add(new Products(33, "31 December, 23:44 ", "Domestication of Fire"));
         mProductlist.add(new Products(34, "31 December, 23:52 ", "Anatomically Modern Humans"));
         mProductlist.add(new Products(35, "31 December, 23:55 ", "Beginning of Most Recent Glacial Period"));
         mProductlist.add(new Products(36, "31 December, 23:58 ", "Sculpture and Painting"));
         mProductlist.add(new Products(37, "31 December, 23:59:32", "Agriculture"));
         mProductlist.add(new Products(38, "\nBeginning of History", null));
         mProductlist.add(new Products(39, "31 December, 23:59:33 ", "End of the Ice Age"));
         mProductlist.add(new Products(40, "31 December, 23:59:41", "Flooding of Doggerland"));
         mProductlist.add(new Products(41, "31 December, 23:59:46", "Chalcolithic"));
         mProductlist.add(new Products(42, "31 December, 23:59:47", "Early Bronze Age; Proto-writing; Building of Stonehenge Cursus"));
         mProductlist.add(new Products(43, "31 December, 23:59:48", "First Dynasty of Egypt, Early Dynastic Period in Sumer, Beginning of Indus Valley Civilisation"));
         mProductlist.add(new Products(44, "31 December, 23:59:49", "Alphabet, Akkadian Empire, Wheel"));
         mProductlist.add(new Products(45, "31 December, 23:59:51", "Code of Hammurabi, Middle Kingdom of Egypt"));
         mProductlist.add(new Products(46, "31 December, 23:59:52", "Late Bronze Age to Early Iron Age; Minoan eruption"));
         mProductlist.add(new Products(47, "31 December, 23:59:53", "Iron Age; Beginning of Classical Antiquity"));
         mProductlist.add(new Products(48, "31 December, 23:59:54", "Buddha, Mahavira, Zoroaster, Confucius, Qin Dynasty, Classical Greece, Ashokan Empire, Vedas Completed, Euclidean geometry, Archimedean Physics, Roman Republic"));
         mProductlist.add(new Products(49, "31 December, 23:59:55", "Ptolemaic astronomy, Roman Empire, Christ, Invention of Numeral 0, Gupta Empire"));
         mProductlist.add(new Products(50, "31 December, 23:59:56", "Muhammad, Maya civilization, Song Dynasty, Rise of Byzantine Empire"));
         mProductlist.add(new Products(51, "31 December, 23:59:58", "Mongol Empire, Maratha Empire, Crusades, Christopher Columbus Voyages to the Americas, Renaissance in Europe, Classical Music to the Time of Johann Sebastian Bach"));
         mProductlist.add(new Products(52, "\nThe Current Second", null));
         mProductlist.add(new Products(53, "31 December, 23:59:59", "Modern History; the last 437.5 years before present."));
         adapter = new CalendarListAdapter(getApplicationContext(),  mProductlist);
         lv.setAdapter( adapter);
    }

 public boolean onCreateOptionsMenu(Menu menu) {
  getMenuInflater().inflate(R.menu.cosmicinfomenu, menu);
  return true;
 }

 public boolean onOptionsItemSelected(MenuItem item) {
  if (item.getItemId() != R.id.menu_info_cosmic_calendar) {
   return super.onOptionsItemSelected(item);
  }
  AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
  builder1.setTitle("Credits:");
  builder1.setMessage("https://en.wikipedia.org/wiki/Cosmic_Calendar\n\nThis calendar visualises the life of universe shrunk into one year for perspective.");
  builder1.create().show();


  return true;
 }

}