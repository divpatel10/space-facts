package com.divyanshu.spacefacts;

public class Products {

    String date;
    private int id;
    String info;


    public Products(int id, String date, String info) {
        this.id = id;
        this.date = date;
        this.info = info;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
