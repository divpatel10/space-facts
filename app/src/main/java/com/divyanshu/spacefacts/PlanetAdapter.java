package com.divyanshu.spacefacts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.divyanshu.spacefacts.planet;
import com.divyanshu.spacefacts.R;
import java.util.ArrayList;

public class PlanetAdapter extends ArrayAdapter<planet> {
    int d_or_y = 0;

    public PlanetAdapter(@NonNull Context context, ArrayList<planet> arrayList, int d) {
        super(context, 0, arrayList);
        this.d_or_y = d;
    }

    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listViewItem = convertView;
        planet p = (planet) getItem(position);
        if (listViewItem == null) {
            listViewItem = LayoutInflater.from(getContext()).inflate(R.layout.calculators_listview, parent, false);
        }
        ((TextView) listViewItem.findViewById(R.id.planet_name)).setText(p.getPlanet());
        TextView tv3 = (TextView) listViewItem.findViewById(R.id.planet_details);
        switch (this.d_or_y) {
            case 0:
                tv3.setText(String.format("%s Years", String.valueOf(p.getYear())));
                break;
            case 1:
                tv3.setText(String.format("%s Days", String.valueOf(p.getDay())));
                break;
            case 2:
                tv3.setText(String.format(String.valueOf(p.getYear())));
                break;
        }
        ((ImageView) listViewItem.findViewById(R.id.planet_image)).setImageResource(p.getImage());
        return listViewItem;
    }
}
