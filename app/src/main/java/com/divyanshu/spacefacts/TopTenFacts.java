package com.divyanshu.spacefacts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class TopTenFacts extends AppCompatActivity {

    ArrayAdapter<String> arrayAdapter;
    String[] planets = new String[]{"Mercury", "Venus", "Earth", "Moon", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune", "Pluto"};
    //String[] topics = new String[]{"Fermi Paradox", "Article 2", "Article 3", "Article 4", "Article 5", "Article 6"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_ten_facts);

        final int J = getIntent().getIntExtra("1", 1);
        ListView listView = (ListView) findViewById(R.id.toptenfacts_listView);
        if (J == 1) {
            arrayAdapter = new ArrayAdapter(this, R.layout.listview_quick_facts_list, planets);
        }
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                if(J==1){
                    intent = new Intent(TopTenFacts.this, FastFactsActivity.class);
                    intent.putExtra("getExtra",position);
                startActivity(intent);
                }

            }
        });

    }
}