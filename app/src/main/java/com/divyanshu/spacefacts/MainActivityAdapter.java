package com.divyanshu.spacefacts;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

public class MainActivityAdapter extends ArrayAdapter<String> {
    ArrayList<String> topics;
    int[] background_resID = {R.drawable.just_stars, R.drawable.planet_storm, R.drawable.thesky, R.drawable.earth_image, R.drawable.forest_stars, R.drawable.astronaut};
    int num_of_images = background_resID.length;
    TextView tv;
    public MainActivityAdapter(@NonNull Context context, ArrayList<String> arrayList) {
        super(context, 0,arrayList);
        topics = arrayList;

    }


    @NonNull
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listViewAdapter = convertView;

        if (listViewAdapter == null) {
            listViewAdapter = LayoutInflater.from(getContext()).inflate(R.layout.list_view_start_menu, parent, false);
        }

        tv =(TextView)  listViewAdapter.findViewById(R.id.textView);
        tv.setText(topics.get(position));
        ImageView imageView = (ImageView) listViewAdapter.findViewById(R.id.imageView);
        imageView.setImageResource(background_resID[position%num_of_images]);



        return listViewAdapter;
    }
}
