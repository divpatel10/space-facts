package com.divyanshu.spacefacts;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;


public class AgeCalculator extends AppCompatActivity {

    String[] planetNamesString = {"Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune", "Pluto"};
    private enum planetNames {
        MERCURY(58.646,0.241),
        VENUS(243.01,0.6156),
        EARTH(1,1),
        MARS(1.026,1.8808),
        JUPITER(0.4135, 11.8626),
        SATURN(0.444, 29.4474),
        URANUS(0.718, 84.01684),
        NEPTUNE(0.671, 164.7913),
        PLUTO(6.387, 247.9206);

        private final double revolutions;
        private final double rotations;

        planetNames(double revolutions, double rotations){
            this.revolutions = revolutions;
            this.rotations = rotations;

            }

        private double revolutions(){
            return revolutions;

        }
        private double rotations(){
            return rotations;

        }


    };


    private double[] planetValuesinDays = new double[9];
    private double[] planetValuesinYears = new double[9];
    int sortType = 0;
    ArrayList<planet> planetArrayList;
    double raw_age;
    ListView listView;
    Calendar calendar1 = Calendar.getInstance();

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            int startYear = selectedYear;
            int startMonth = selectedMonth;
            int startDay = selectedDay;

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH, startDay);
            calendar.set(Calendar.MONTH, startMonth);
            calendar.set(Calendar.YEAR, startYear);

                calendar1.get(Calendar.DAY_OF_MONTH);
                calendar1.get(Calendar.MONTH);
                calendar1.get(Calendar.YEAR);

                if (calendar1 == null || calendar == null) {
                raw_age = 0.0;
            }
            if ((calendar1.getTimeInMillis() - calendar.getTimeInMillis()) / 86400000 >= 0) {
                raw_age = (double) ((calendar1.getTimeInMillis() - calendar.getTimeInMillis()) / 86400000);
            } else {
                raw_age = 0.0;
                Toast.makeText(AgeCalculator.this, "Seems like you're from the future!\n Unfortunately, this app is not...", Toast.LENGTH_SHORT).show();
            }

            planetArrayList = new ArrayList<>();
            int[] planetImages = {R.drawable.mercury, R.drawable.venus, R.drawable.earth, R.drawable.mars, R.drawable.jupiter, R.drawable.saturn, R.drawable.uranus, R.drawable.neptune, R.drawable.pluto};

            for(int i=0; i<9; i++){
                planetValuesinDays[i] = raw_age/planetNames.values()[i].revolutions();
                planetValuesinYears[i] = (raw_age/365.0)/ planetNames.values()[i].rotations();
                planetArrayList.add(new planet(planetNamesString[i],planetValuesinDays[i], planetValuesinYears[i],planetImages[i]));
            }
            set_List();
        }
    };
    
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_age_calculator);
        ( findViewById(R.id.age_activity_fab)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new DatePickerDialog(AgeCalculator.this, mDateSetListener, calendar1.get(Calendar.YEAR), calendar1.get(Calendar.MONTH), calendar1.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

    }


    private void set_List(){

        listView = findViewById(R.id.age_activity_listview);
        listView.setAdapter(new PlanetAdapter(AgeCalculator.this, planetArrayList, sortType));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.age_nav, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.sort_type) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setTitle("Age format");
            builder1.setCancelable(true);
            builder1.setPositiveButton("Years", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    sortType = 0;
                    if (listView != null) {
                        set_List();
                    } else {
                        Toast.makeText(AgeCalculator.this, "Done!", Toast.LENGTH_SHORT).show();
                    }
                    dialog.cancel();
                }
            });
            builder1.setNegativeButton("Days", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    sortType = 1;
                    if (listView != null) {
                        set_List();
                    } else {
                        Toast.makeText(AgeCalculator.this, "Done!", Toast.LENGTH_SHORT).show();
                    }
                    dialog.cancel();
                }
            });
            builder1.create().show();
        }
        return true;
    }
}
