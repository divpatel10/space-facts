package com.divyanshu.spacefacts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    String[] topics = new String[]{"Facts", "Quick Facts", "Age Calculator", "Weight Calculator", "Cosmic Calendar", "Send Feedback"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(R.id.start_list);

        ArrayList<String> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, topics);

        ArrayAdapter arrayAdapter = new MainActivityAdapter(this, arrayList);
       // ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.list_view_start_menu, arrayList);

        listView.setAdapter(arrayAdapter);

        //ListView Click handling
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(position==0){
                   startActivity(new Intent(MainActivity.this ,SpaceFacts.class));
                }
                else if (position ==1){
                    startActivity(new Intent(MainActivity.this ,TopTenFacts.class));
                }
                else if (position ==2){
                    startActivity(new Intent(MainActivity.this ,AgeCalculator.class));
                }
                else if (position ==3){
                    startActivity(new Intent(MainActivity.this ,WeightCalculator.class));

                }else if (position ==4){
                    startActivity(new Intent(MainActivity.this ,CosmicCalendar.class));

                }else if (position ==5){
                    Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto","nautstradamus@gmail.com", null));
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Space Facts App Suggestion");
                    startActivity(Intent.createChooser(emailIntent, "Send email..."));

                }
            }
        });

    }


}