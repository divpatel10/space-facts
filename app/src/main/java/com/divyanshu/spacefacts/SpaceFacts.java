package com.divyanshu.spacefacts;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.List;


public class SpaceFacts extends AppCompatActivity {

    int i=0;
    TextView theFacts;
    TextView factNumbers;
    SharedPreferences sharedPreferences;
    FactsList spaceFacts;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_space_facts);

         theFacts = findViewById(R.id.space_facts_sentences);
         factNumbers = findViewById(R.id.fact_numbers);


         //shared prefrences to save last viewed fact
        sharedPreferences = getSharedPreferences("mypref", 0);
        i = sharedPreferences.getInt("spsave", 0);


        ConstraintLayout swipeElement = findViewById(R.id.swipeElement);  //swipeElement is the whole activity layout here
         spaceFacts = new FactsList(getApplicationContext());
        factNumbers.setText(String.valueOf(i + 1));
        theFacts.setText(spaceFacts.getFact(i));

        //User Swipe Handling
        swipeElement.setOnTouchListener(new SwipeEvents(SpaceFacts.this) {
            public void onSwipeRight() {
                if (i >= 1) {
                    i--;
                    theFacts.setText(spaceFacts.getFact(i));
                } else {
                    Toast.makeText(SpaceFacts.this, "Already the First Fact!", Toast.LENGTH_LONG).show();
                }
                factNumbers.setText(String.valueOf(i + 1));
                Savefact(i);            }
            public void onSwipeLeft() {
                int factlenth = spaceFacts.len();
                if (i < factlenth) {
                    i++;
                    theFacts.setText(spaceFacts.getFact(i));
                }
                else {
                    i = factlenth;
                }
                factNumbers.setText(String.valueOf(i + 1));
                Savefact(i);
            }


        });

        //Swipe handling on the textView widget that handles facts
        theFacts.setOnTouchListener(new SwipeEvents(SpaceFacts.this) {
            public void onSwipeRight() {
                if (i >= 1) {
                    i--;
                    theFacts.setText(spaceFacts.getFact(i));
                } else {
                    Toast.makeText(SpaceFacts.this, "Already the First Fact!", Toast.LENGTH_LONG).show();
                }
                factNumbers.setText(String.valueOf(i + 1));
                Savefact(i);            }
            public void onSwipeLeft() {
                int factlenth = spaceFacts.len();
                if (i < factlenth) {
                    i++;
                    theFacts.setText(spaceFacts.getFact(i));
                }
                else {
                    i = factlenth;
                }
                factNumbers.setText(String.valueOf(i + 1));
                Savefact(i);
            }
        } );

    }

    //Function to save the fact using shared Preferences
    public void Savefact(int i){
        SharedPreferences sharedPreferences = getSharedPreferences("mypref", 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("spsave", i);
        editor.apply();
        }


    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.facts, menu);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //This if handles the "share" icon touch event, to create a alertbox for user to jump to a certain fact number
        if (id == R.id.menu_searchFact) {
            final EditText edittext = new EditText(this);
            edittext.setInputType(2);
            edittext.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Go to Fact number...");
            alert.setView(edittext);
            alert.setPositiveButton("Go", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {


                    int inputNumber = Integer.parseInt(edittext.getText().toString());
                    if (inputNumber < 1 || inputNumber>(spaceFacts.len())) {
                        Toast.makeText(SpaceFacts.this.getApplicationContext(), "Please try a valid input", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    i = inputNumber;
                    factNumbers.setText(String.valueOf(i));
                    theFacts.setText(spaceFacts.getFact(i - 1));
                   // FactsActivity factsActivity = FactsActivity.this;
                    i--;
                    Savefact(i);
                }
            });
            alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alert.create().show();

            //This if handles share touch event that uses intent to share facts
        } else if (id == R.id.menu_share_facts) {
            String sharedFact = theFacts.getText().toString();
            Intent sharingIntent = new Intent("android.intent.action.SEND");
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra("android.intent.extra.TEXT", sharedFact);
            startActivity(Intent.createChooser(sharingIntent, "Share Fact Via"));
        }
        return super.onOptionsItemSelected(item);
    }

}