package com.divyanshu.spacefacts;

import java.text.DecimalFormat;

public class planet {
    private double date;
    private double day;
    private int image;
    private String planet;

    public planet(String planet, double day, double date, int image) {
        this.planet = planet;
        this.date = date;
        this.day = day;
        this.image = image;
    }

    public planet(String planet, double weight, int image) {
        this.planet = planet;
        this.date = weight;
        this.image = image;
    }

    public String getPlanet() {
        return this.planet;
    }

    public double getDay() {
        return Double.parseDouble(new DecimalFormat("##.##").format(this.day));
    }

    public double getYear() {
        return Double.parseDouble(new DecimalFormat("##.##").format(this.date));
    }

    public int getImage() {
        return this.image;
    }
}
